﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour 
{
	public const string player = "Player";
	public const string gameController = "GameController";
	public static string daylight = "DayLight";
	public const string landscape = "Landscape";
	public static string landPart = "LandPart";

	public const string command_choiseClick = "command_choiseClick";
	public const string command_moveToPoint = "command_moveToPoint";


	public const string plant_potion_health = "healthPotion";
	public const string plant_potion_energy = "energyPotion";
	public const string plant_poison_health = "healthPoison";
	public const string plant_poison_energy = "energyPoison";


	public const string action_eat = "action_eat";
	public const string action_take = "action_take";
	public const string action_leave = "action_leave";



}
