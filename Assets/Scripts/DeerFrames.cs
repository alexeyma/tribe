﻿using UnityEngine;
using System.Collections;

public class DeerFrames : MonoBehaviour {
	
	public Sprite[] framesIdle;
	public Sprite[] framesHear;
	public Sprite[] framesRun;
	public Sprite[] framesDeath;

	private Sprite[] frames;
	public float framesPerSecond;
	private int changes = 0;
	private int lastIndex = 0;
	private bool needStop = false;
	// Use this for initialization
	void Awake () {
		frames = framesIdle;
		GetComponent<SpriteRenderer>().sprite = frames[0];

	}

	void Update () {
		if (!needStop)
		{
			int index = Mathf.FloorToInt(Time.time * framesPerSecond);
			index = index % frames.Length;
			GetComponent<SpriteRenderer>().sprite = frames[index];
		} else 
		{
			int index = Mathf.FloorToInt(Time.time * framesPerSecond);
			index = index % frames.Length;
			if(frames[index]!=null)
			{
				GetComponent<SpriteRenderer>().sprite = frames[index];
				frames[index] = null;
			}
		}
	}
	
	public void ChangeState(string newState)
	{
		Debug.Log(newState);
		switch (newState)
		{
		case "hear":
			if(frames == framesIdle)
			{
				frames = framesHear;
				needStop = true;
				lastIndex = 0;
			}
			break;
		case "unhear":
			if(frames == framesHear)
			{
				frames = framesIdle;
				needStop = false;
				lastIndex = 0;
			}
			break;
		case "run":
			if(frames != framesRun)
			{
				frames = framesRun;
				needStop = false;
			}
			break;
		case "hunt":
			if(frames != framesDeath)
			{
				frames = framesDeath;
				needStop = true;
			}
			break;
		}
	}
	
	
}
