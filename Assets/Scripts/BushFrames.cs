﻿using UnityEngine;
using System.Collections;

public class BushFrames : MonoBehaviour {

	public Sprite[] frames;
	// Use this for initialization
	void Awake () {
		GetComponent<SpriteRenderer>().sprite = frames[0];
	}
	
	public void ChangeState(string newState)
	{
		Debug.Log(newState);
		switch (newState)
		{
		case "eat":
			GetComponent<SpriteRenderer>().sprite = frames[1];
			break;
		}
	}
}
