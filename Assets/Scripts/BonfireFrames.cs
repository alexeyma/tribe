﻿using UnityEngine;
using System.Collections;

public class BonfireFrames : MonoBehaviour {

	public Sprite[] frames;
	// Use this for initialization
	void Awake () {
		GetComponent<SpriteRenderer>().sprite = frames[0];
	}

	public void ChangeState(string newState)
	{
		Debug.Log(newState);
		switch (newState)
		{
		case "leave":
			GetComponent<SpriteRenderer>().sprite = frames[1];
			break;
		case "dead":
			GetComponent<SpriteRenderer>().sprite = frames[2];
			break;
		case "win":
			GetComponent<SpriteRenderer>().sprite = frames[3];
			break;
		}
	}
	

}
