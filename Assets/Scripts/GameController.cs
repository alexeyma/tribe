﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour 
{

	public GameObject manClass;
	public GameObject backgroundClass;
	public GameObject riverClass;
	public GameObject bridge1Class;
	public GameObject bridge2Class;

	public GameObject bonfire5Class;

	public Camera camera;

	public float foodTime;
	private float timer;

	private ArrayList tribe;
	private Object bg;
	//private GameObject shaman;



	private int size = 50;
	private float defaultZ = 0;
	private int numGroups = 1;
	private float screenWidth = 17.88f;
	private float speed = 2f;
	private float food = 100f;
	private float eat = 0.2f;
	private float lastFood = 0;

	public int[] groupsSize;



	void Start()
	{

		tribe = new ArrayList();
		CreateTribe();
		bg = Instantiate(backgroundClass,new Vector3(0,0,0),Quaternion.identity);

		float yPosition = Random.Range (-5f,-10f);
		float xPosition = 0f;
		Object[] levelDecor = (bg as GameObject).GetComponent<Background>().GetDecorations();
		for(int j=0;j<100;j++)
		{
			float newR = Random.Range(0f,1f);
			if(newR<0.5)
			{
				xPosition = Random.Range (0f,0.7f);
				xPosition = 0.2f;
			} else 
			{
				xPosition = screenWidth-1f;
				xPosition = Random.Range (screenWidth-1.5f,screenWidth-0.4f);
			}
			Vector3 spawnPosition = new Vector3 (xPosition,yPosition,-2);
			Quaternion spawnRotation = Quaternion.identity;
			Object decorClass = levelDecor[Mathf.FloorToInt(Random.Range(1,levelDecor.Length+1)-1)];
			var newDecor = Instantiate(decorClass,spawnPosition,spawnRotation);
			yPosition -= Random.Range(3f,10f);
		}
		yPosition = Random.Range (-6f,-20f);
		for(int i=0;i<50;i++)
		{
			float random = Random.Range(0f,1f);
			if(random<0.1)
			{
				Vector3 spawnPosition = new Vector3 (0,yPosition,0);
				Quaternion spawnRotation = Quaternion.identity;
				var newRiver = Instantiate(riverClass,spawnPosition,spawnRotation);				
				var newBridge1 = Instantiate(bridge1Class,spawnPosition,spawnRotation);
			} else if (random<0.2)
			{
				Vector3 spawnPosition = new Vector3 (0,yPosition,0);
				Quaternion spawnRotation = Quaternion.identity;
				var newRiver = Instantiate(riverClass,spawnPosition,spawnRotation);				
				var newBridge1 = Instantiate(bridge2Class,spawnPosition,spawnRotation);
			} else if (random<0.3)
			{
				Vector3 spawnPosition = new Vector3 (Random.Range(screenWidth-0.9f*screenWidth,.9f*screenWidth),yPosition,0);
				Quaternion spawnRotation = Quaternion.identity;
				Object bushClass = (bg as GameObject).GetComponent<Background>().GetVegetable();
				var newBush = Instantiate(bushClass,spawnPosition,spawnRotation);
			} else if (random<0.4)
			{
				Vector3 spawnPosition = new Vector3 (Random.Range (screenWidth-0.9f*screenWidth,.9f*screenWidth),yPosition,0);
				Quaternion spawnRotation = Quaternion.identity;
				var newBonfire = Instantiate(bonfire5Class,spawnPosition,spawnRotation);
			} else  
			{
				Vector3 spawnPosition = new Vector3 (Random.Range(screenWidth-0.9f*screenWidth,.9f*screenWidth),yPosition,0);
				Quaternion spawnRotation = Quaternion.identity;
				Object animalClass = (bg as GameObject).GetComponent<Background>().GetHuntingAnimal();
				var newAnimal = Instantiate(animalClass,spawnPosition,spawnRotation);
			}
			yPosition -= Random.Range(8f,10f);
		}


		/*Vector3 spawnPosition = new Vector3 (0,Random.Range (-5f,-10f),0);
		Quaternion spawnRotation = Quaternion.identity;
		var newRiver = Instantiate(riverClass,spawnPosition,spawnRotation);

		var newBridge1 = Instantiate(bridge1Class,spawnPosition,spawnRotation);

		for(int i=0;i<20;i++)
		{
			spawnPosition = new Vector3 (Random.Range (0,screenWidth),Random.Range (-12f*i/2,-15f*i/2),0);
			spawnRotation = Quaternion.identity;
			Object bushClass = (bg as GameObject).GetComponent<Background>().GetVegetable();
			var newBush = Instantiate(bushClass,spawnPosition,spawnRotation);
		}

		for(int i=0;i<5;i++)
		{
			spawnPosition = new Vector3 (Random.Range (0,screenWidth),Random.Range (-20 -12f*i/2,-20 -15f*i/2),0);
			spawnRotation = Quaternion.identity;
			var newBonfire = Instantiate(bonfire5Class,spawnPosition,spawnRotation);
		}*/
	}



	void CreateTribe()
	{
		for (int i=0;i<size;i++)
		{
			//Debug.Log(i);
			Vector3 spawnPosition = new Vector3 (Random.Range (7f-0.5f, 7f+0.5f),Random.Range (-0.5f, -1.5f),0);
			Quaternion spawnRotation = Quaternion.identity;
			var newMan = Instantiate(manClass,spawnPosition,spawnRotation);
			tribe.Add(newMan);

			//(newMan as GameObject).GetComponent<Man>().makeShaman();
		}

		(tribe[Mathf.FloorToInt(tribe.Count/2)] as GameObject).GetComponent<Man>().makeShaman();
		//shaman = tribe[Mathf.FloorToInt(tribe.Count/2)] as GameObject;
		CreateGroups(-1f);
	}

	public void AddToTribe(float amount)
	{
		for (int i=0;i<amount;i++)
		{
			//Debug.Log(i);
			Vector3 spawnPosition = (tribe[0] as GameObject).GetComponent<Man>().transform.position;
			Quaternion spawnRotation = Quaternion.identity;
			var newMan = Instantiate(manClass,spawnPosition,spawnRotation);
			tribe.Add(newMan);
			(newMan as GameObject).GetComponent<Man>().ChangeArms((tribe[0] as GameObject).GetComponent<Man>().GetTool());

		}
		CreateGroups(-1f);
	}

	void CreateGroups(float clickX)
	{

		if (numGroups > tribe.Count)
		{

			numGroups = tribe.Count;
		} else if (numGroups <1)
		{
			numGroups = 1;
		} 
		groupsSize = new int[numGroups];
		//Debug.Log(numGroups);
		float step = screenWidth/(numGroups+1f);
		int currentGroup = 1;
		foreach( GameObject man in tribe)
		{
			float randY = camera.transform.position.y + 4.9f + Random.Range (-2f, -1f);
			float localY = (randY - camera.transform.position.y)*-1f;
			man.GetComponent<Man>().MoveBody(new Vector3 (Random.Range (currentGroup*step-step/7f, currentGroup*step+step/7f), randY, defaultZ+(5f/localY)));
			man.GetComponent<Man>().SetGroup(currentGroup-1);
			groupsSize[currentGroup-1]++;
			currentGroup++;
			if(currentGroup>numGroups) currentGroup = 1;
		}
		//if(clickX!=-1f)
		//{
		//	shaman.GetComponent<Man>().MoveBody(new Vector3 (clickX,camera.transform.position.y + 4.9f + Random.Range (-0.5f, -1f),shaman.GetComponent<Man>().transform.position.z));
		//}
	}

	public void DeathMan(GameObject man, string deathType="")
	{
		bool isShamanDead = false;
		foreach( GameObject member in tribe)
		{
			if(member == man)
			{
				//Debug.Log("FIND AND DELETE MAN");
				if (member.GetComponent<Man>().isShaman) { isShamanDead = true; } 
				member.GetComponent<Man>().MakeDead(deathType);
				tribe.Remove(member);
				GameObject.Destroy(member);
				break;
			}
		}
		if (isShamanDead) 
		{
			(tribe[Mathf.FloorToInt(tribe.Count/2)] as GameObject).GetComponent<Man>().makeShaman();
			//shaman = tribe[Mathf.FloorToInt(tribe.Count/2)] as GameObject;
		}
	}

	public void DeathRandomMan()
	{
		float randomMan = Random.Range(0,tribe.Count);
		GameObject member = (tribe[Mathf.FloorToInt(randomMan)] as GameObject);


		//Debug.Log("FIND AND DELETE MAN");
		if(!member.GetComponent<Man>().isShaman || tribe.Count==1)
		{
			member.GetComponent<Man>().MakeDead("starving");
			tribe.Remove(member);
			GameObject.Destroy(member);
		}
	}

	public void DeathGroup(int index, string deathType="")
	{
		bool isShamanDead = false;
		ArrayList membersToDelete = new ArrayList();
		foreach( GameObject member in tribe)
		{
			if(member.GetComponent<Man>().GetGroup() == index)
			{
				//Debug.Log("FIND AND DELETE MAN");
				if (member.GetComponent<Man>().isShaman) { isShamanDead = true; } 

				membersToDelete.Add(member);


			}
		}
		foreach(GameObject member in membersToDelete)
		{

			member.GetComponent<Man>().MakeDead(deathType);
			tribe.Remove(member);
			GameObject.Destroy(member);
		}
		if (isShamanDead) 
		{
			(tribe[Mathf.FloorToInt(tribe.Count/2)] as GameObject).GetComponent<Man>().makeShaman();
			//shaman = tribe[Mathf.FloorToInt(tribe.Count/2)] as GameObject;
		}
	}

	public void ChangeTribeArms(string status)
	{
		foreach( GameObject man in tribe)
		{
			man.GetComponent<Man>().ChangeArms(status);
		}
	}

	public void AddFood(float howmany)
	{
		food+= howmany;
		Debug.Log(food);
	}


	void Update()
	{
		LeftMouseCheck();
		RightMouseCheck();
		KeyboardCheck();
		camera.transform.position += new Vector3 (0,-speed*Time.deltaTime,0);
		//foreach( GameObject man in tribe)
		//{
		//	man.GetComponent<Man>().MakeStep(new Vector3 (0,-speed,0));
		//}
		timer+=Time.deltaTime;
		if(timer>=foodTime)
		{
			FoodAction();
			timer = 0;
		}

	}

	void KeyboardCheck()
	{
		if (Input.GetButtonDown("action1"))
		{
			ChangeTribeArms("empty");
		} else if (Input.GetButtonDown("action2"))
		{
			ChangeTribeArms("weapon");
		} else if (Input.GetButtonDown("action3"))
		{
			ChangeTribeArms("torch");
		}
	}

	void FoodAction()
	{
		food -= eat*tribe.Count;

		if(food<0)
		{
			food = 0;
			DeathRandomMan();
		}
		//Debug.Log(food);
	}


	
	void LeftMouseCheck()
	{
		if (Input.GetMouseButtonDown(0))
		{
			numGroups--;
			float clickX = screenWidth*(Input.mousePosition.x/Screen.width);
			CreateGroups(clickX);
		}
	}


	void RightMouseCheck()
	{
		if (Input.GetMouseButtonDown(1))
		{
			numGroups++;
			float clickX = screenWidth*(Input.mousePosition.x/Screen.width);
			CreateGroups(clickX);
		}
	}





}
