﻿using UnityEngine;
using System.Collections;

public class Man : MonoBehaviour {

	public GameObject defaultBody;
	public GameObject shamanBody;
	public GameObject weaponBody;
	public GameObject torchBody;

	public GameObject corpseStarving;
	public GameObject corpseRiver;
	public GameObject corpseEnemy;

	public bool isAlive = true;

	private Object body;


	private Vector3 nextPosition;
	private string currentTool = "hand";
	private float speed = 2f;
	private int group;

	public bool isShaman = false;
	
	//private Quaternion spawnRotation;

	void Awake () {
		//spawnPosition = new Vector3 (0, 0, 0);
		//spawnRotation = Quaternion.identity;
		body = Instantiate (defaultBody, this.transform.position, this.transform.rotation);

	}
	
	// Update is called once per frame
	void Update () {
		if(isAlive)
		{
			nextPosition = nextPosition + new Vector3 (0,-speed*Time.deltaTime,0);
			if(Vector2.Distance(nextPosition, this.transform.position)>0.1f)
			{
				Vector3 direction = nextPosition - this.transform.position ;
				this.transform.position = this.transform.position+direction*Time.deltaTime*8f;
				(body as GameObject).transform.position = this.transform.position;
			}
		}

	}

	public void makeShaman()
	{
		DestroyBody();
		body = Instantiate (shamanBody, this.transform.position, this.transform.rotation);
		isShaman = true;
		currentTool = "hand";
		//this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z+5f);
		//(body as GameObject).transform.position = this.transform.position;
	}

	public void ChangeArms(string status)
	{
			switch (status)
			{
			case "empty":
				SetHands();
				break;
			case "weapon":
				SetWeapon();
				break;
			case "torch":
				SetTorch();
				break;
			}
	}

	public void SetTorch()
	{
		currentTool = "torch";
		if(!isShaman)
		{
			DestroyBody();
			body = Instantiate (torchBody, this.transform.position, this.transform.rotation);

		}
	}

	public void SetWeapon()
	{
		currentTool = "weapon";
		if(!isShaman)
		{
			DestroyBody();
			body = Instantiate (weaponBody, this.transform.position, this.transform.rotation);

		}
	}

	public void SetHands()
	{
		currentTool = "hand";
		if(!isShaman)
		{
			DestroyBody();
			body = Instantiate (defaultBody, this.transform.position, this.transform.rotation);

		}
	}

	public void MoveBody(Vector3 newPosition)
	{
		nextPosition = newPosition;

	}

	public void MakeStep(Vector3 newPosition)
	{
		nextPosition = nextPosition + newPosition;
		
	}

	public void MakeDead(string reason)
	{
		if(isAlive)
		{
			DestroyBody();
			switch (reason)
			{
			case "starving":
				body = Instantiate (corpseStarving, this.transform.position, this.transform.rotation);
				break;
			case "river":
				body = Instantiate (corpseRiver, this.transform.position, this.transform.rotation);
				break;
			case "enemy":
				body = Instantiate (corpseEnemy, this.transform.position, this.transform.rotation);
				break;
			}
			isAlive = false;
			gameObject.collider.enabled = false;
			GameObject.Destroy(this.gameObject);
		}

	}

	private void DestroyBody()
	{
		GameObject.Destroy((body as GameObject).GetComponent<FramePlayer>());
		GameObject.Destroy(body);

	}

	public string GetTool()
	{
		return currentTool;
	}

	public void SetGroup(int num)
	{
		group = num;
	}

	public int GetGroup()
	{
		return group;
	}
}
