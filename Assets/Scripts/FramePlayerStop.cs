﻿using UnityEngine;
using System.Collections;

public class FramePlayerStop : MonoBehaviour {
	
	public Sprite[] frames;
	public float framesPerSecond = 10f;
	private int changes = 0;
	
	void Awake()
	{
		//framesPerSecond+=Random.Range(-1f, 1f);
	}
	
	void Update () {
		if (changes>=0)
		{
			int index = Mathf.FloorToInt(Time.time * framesPerSecond);
			index = index % frames.Length;
			GetComponent<SpriteRenderer>().sprite = frames[index];
			changes++;
			if(changes>=frames.Length)
			{
				changes = -1;
			}

		}
	}
}
