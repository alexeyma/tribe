﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

	public Object desertSkin;
	public Object snowSkin;
	public Object grassSkin;
	public Object[] grassDecorations;

	public Object snowBush;

	public Object grassDeer;

	private Object skin;
	private string currentSkin = "grass";

	private Object body;
	private Object body2;


	public float vertical;// = 10f;
	// Use this for initialization
	void Awake () {
		CreateSkin(currentSkin);

	}

	void CreateSkin(string current)
	{

		if(body)
		{
			GameObject.Destroy(body);
			//GameObject.Destroy(body2);
		}
		switch(current)
		{
		case "desert":
			skin = desertSkin;
			break;
		case "snow":
			skin = snowSkin;
			break;
		case "grass":
			skin = grassSkin;
			break;
		}


		Vector3 spawnPosition = new Vector3 (0,0,0);
		Quaternion spawnRotation = Quaternion.identity;
		body = Instantiate(skin,spawnPosition,spawnRotation);
		//body2 = Instantiate(skin,spawnPosition+new Vector3(0,-vertical,0),spawnRotation);
	}

	public Object GetVegetable()
	{
		Object vegetable = new Object();
		switch(currentSkin)
		{
		case "desert":
			vegetable = snowBush;
			break;
		case "snow":
			vegetable = snowBush;
			break;
		case "grass":
			vegetable = snowBush;
			break;
		}

		return vegetable;
	}

	public Object GetHuntingAnimal()
	{
		Object animal = new Object();
		switch(currentSkin)
		{
		case "desert":
			animal = grassDeer;
			break;
		case "snow":
			animal = grassDeer;
			break;
		case "grass":
			animal = grassDeer;
			break;
		}
		
		return animal;
	}

	public Object[] GetDecorations()
	{

		switch (currentSkin)
		{
		case "desert":
			return grassDecorations;
			break;
		case "snow":
			return grassDecorations;
			break;
		case "grass":
			return grassDecorations;
			break;
		default:
			return grassDecorations;
			break;
		}
	}

}
