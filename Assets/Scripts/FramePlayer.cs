﻿using UnityEngine;
using System.Collections;

public class FramePlayer : MonoBehaviour {

	public Sprite[] frames;
	public float framesPerSecond = 10f;

	void Awake()
	{
		framesPerSecond+=Random.Range(-1f, 1f);
	}
	
	void Update () {
		int index = Mathf.FloorToInt(Time.time * framesPerSecond);
		index = index % frames.Length;

		GetComponent<SpriteRenderer>().sprite = frames[index];
		//renderer.material.mainTexture = frames[index];
	}
}
