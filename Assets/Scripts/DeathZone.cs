﻿using UnityEngine;
using System.Collections;

public class DeathZone : MonoBehaviour {


	protected GameController game;
	protected bool isActivated = false;
	public string action;
	public string type;
	public float amount;

	private int menInZone = 0;
	// Use this for initialization
	public void Awake () {
		Debug.Log(action + "zone awake");
		game = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<GameController>();
	}
	
	public void OnTriggerEnter(Collider other)
	{
		if(!isActivated){
			Debug.Log(action + "detect Some" + " " + other.gameObject.tag);
			if(other.gameObject.tag == "man")
			{
				//Debug.Log("detect player");
				//lastPlayerSighting.position = other.transform.position;
				ZoneAction(other.gameObject);

			}
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if(!isActivated){
			Debug.Log(action + "left Some" + " " + other.gameObject.tag);
			if(other.gameObject.tag == "man")
			{
				//Debug.Log("detect player");
				//lastPlayerSighting.position = other.transform.position;
				ZoneExit(other.gameObject);
				
			}
		}
	}

	public void ZoneAction(GameObject obj)
	{
		Man man = obj.GetComponent<Man>();
		switch (action)
		{
		case "death":
			game.DeathMan(obj,type);
			break;
		case "food":
			if(man.GetTool()=="hand")
			{
				Debug.Log("FoodZoneEnter");
				game.AddFood(amount);
				isActivated = true;
				transform.root.GetChild(0).GetComponent<SpriteRenderer>().GetComponent<BushFrames>().ChangeState("eat");
			}
			break;
		case "hear":
			Debug.Log("hearAction");
			menInZone++;
			HearAction();
			break;
		case "run":
			Debug.Log("runAction");
			menInZone++;
			RunAction();
			break;
		case "hunt":
			Debug.Log("huntAction");
			HuntAction(man);
			break;
		case "bonfire":
			BonfireAction(man);

				break;
		}

	}

	public void ZoneExit(GameObject obj)
	{
		switch (action)
		{
		case "hear":
			menInZone--;
			HearAction();
			break;
		case "run":
			menInZone--;
			break;
		}
	}

	private void HearAction()
	{
		//isActivated = true;
		DeerFrames animal = transform.root.GetChild(0).GetComponent<SpriteRenderer>().GetComponent<DeerFrames>();
		//int groupSize = game.groupsSize[man.GetGroup()];
		Debug.Log("men in zone: " + menInZone);
		if(menInZone>amount)
		{
			animal.ChangeState("hear");
		} else 
		{
			animal.ChangeState("unhear");
		}
	}

	private void RunAction()
	{
		if(menInZone>amount)
		{
			DeerFrames animal = transform.root.GetChild(0).GetComponent<SpriteRenderer>().GetComponent<DeerFrames>();
			isActivated = true;
			animal.ChangeState("run");
		}
	}

	private void HuntAction(Man man)
	{
		if(man.GetTool()=="weapon")
		{
			game.AddFood(amount);
			isActivated = true;
			DeerFrames animal = transform.root.GetChild(0).GetComponent<SpriteRenderer>().GetComponent<DeerFrames>();
			animal.ChangeState("hunt");
		}
	}

	private void BonfireAction(Man man)
	{
		isActivated = true;
		GameObject bonfire = transform.root.gameObject;
		int groupSize = game.groupsSize[man.GetGroup()];
		Debug.Log(groupSize);
		if(man.GetTool()=="weapon")
		{
			if(groupSize>amount)
			{
				transform.root.GetChild(0).GetComponent<SpriteRenderer>().GetComponent<BonfireFrames>().ChangeState("dead");
				game.AddFood(amount*4f);
			} else 
			{
				transform.root.GetChild(0).GetComponent<SpriteRenderer>().GetComponent<BonfireFrames>().ChangeState("win");
				game.DeathGroup(man.GetGroup(),"enemy");
			}
		} else 
		{
			if(groupSize>amount)
			{
				transform.root.GetChild(0).GetComponent<SpriteRenderer>().GetComponent<BonfireFrames>().ChangeState("leave");
				game.AddToTribe(amount);
			} else 
			{
				transform.root.GetChild(0).GetComponent<SpriteRenderer>().GetComponent<BonfireFrames>().ChangeState("win");
				game.DeathGroup(man.GetGroup(),"enemy");
			}
		}
	}
}
